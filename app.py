from flask import Flask, jsonify, request
from factory.ServiceFactory import ServiceFactory
from service.services import *
from service.responseService import create_response, create_failed_response, \
    create_all_data_response, send_css_response, refresh_nginx, send_image_data
from service.imageService import check_image_extension
from service.requestService import update_page, update_contact, update_account, update_assets, update_styles, upload_files
from flask_cors import CORS
from logging.config import fileConfig
import sys

app = Flask(__name__)
app.config['DEBUG'] = True
CORS(app)
fileConfig('logging.cfg')

service_factory = ServiceFactory()
page_service: PageService = service_factory.get_page_service()
contact_service: ContactService = service_factory.get_contact_service()
assets_service: EssentialAssetsService = service_factory.get_assets_service()
css_service: CssDataService = service_factory.get_css_service()
account_service: AccountService = service_factory.get_account_service()


@app.route('/page', methods=['GET'])
def get_page_data():
    try:
        response = create_response(page_service.get_page())
        return response
    except KeyError:
        return create_failed_response()


@app.route('/page/edit', methods=['POST'])
def update_page_data():
    try:
        response = create_response((update_page()))
        return response
    except AttributeError:
        return create_failed_response()


@app.route('/contact')
def get_contact_data():
    try:
        response = create_response(contact_service.get_contact())
        return response

    except KeyError:
        return create_failed_response()


@app.route('/contact/edit', methods=['POST'])
def update_contact_data():
    try:
        response = create_response(update_contact())
        return response
    except AttributeError:
        return create_failed_response()


@app.route('/assets')
def get_essentials_assets():
    try:
        response = create_response(assets_service.get_assets())
        return response
    except KeyError:
        return create_failed_response()


@app.route('/assets/edit', methods=['POST'])
def update_essentials_assets():
    try:
        response = create_response(update_assets())
        return response
    except AttributeError:
        return create_failed_response()


@app.route('/apiadmin/static', methods=['GET'])
def get_all_data():
    try:
        page = create_response(page_service.get_page())
        contact = create_response(contact_service.get_contact())
        assets = create_response(assets_service.get_assets())
        css = css_service.get_theme()
        account = create_response(account_service.get_account())
        data_response = create_all_data_response(
            page, contact, assets, css, account)
        app.logger.info('Successfully returned static data')
        return create_response(data_response)
    except KeyError:
        app.logger.error(
            'Error, failed to send static data. There are misnamed at data.json')
        return create_failed_response()


@app.route('/apiadmin/static', methods=['POST'])
def update_all_data():
    try:
        refresh_nginx()
        update_page()
        update_contact()
        update_styles()
        update_account()
        response = update_assets()
        app.logger.info('Successfully updated static data')
        return create_response(response)
    except KeyError:
        app.logger.error(
            'Error, failed to update static data. Json name not matched')
        return create_failed_response()


@app.route('/apiadmin/css', methods=['GET'])
def get_css():
    try:
        app.logger.info('Trying to send CSS files')
        return send_css_response()
    except KeyError:
        app.logger.error('Error, failed to send CSS')
        create_failed_response()


@app.route('/apiimage/upload', methods=['POST'])
def upload_image():
    return upload_files()


@app.route('/apiimage/<image_name>', methods=['GET'])
def send_image(image_name):
    try:
        return send_image_data(image_name)
    except FileNotFoundError:
        app.logger.error('Image not found')
        return create_failed_response('File Not Found')


@app.route('/apiimage/checkextension', methods=['GET'])
def check_extension():
    image_name = request.args.get("imagename")
    is_allowed = check_image_extension(image_name)
    response = {"data": is_allowed}
    return response


if __name__ == '__main__':
    app.run(debug=True)
