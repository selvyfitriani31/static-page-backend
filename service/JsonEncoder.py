from model.interface.serializable import Serializable

class JsonEncoder():

    def encode(self, obj:Serializable):
        return obj.serialize()

