from interface import implements
from service.services import *
from model.Page import Page
from model.Essential_Assets import EssentialAssets
from model.Contact import Contact
from model.Account import Account
from repository.repository import PageRepository, EssentialAssetsRepository, \
    ContactRepository, CssDataRepository, ProductPropertiesRepository, AccountRepository
from flask import jsonify


class PageServiceImpl(implements(PageService)):

    def __init__(self, page_repository):
        self.page_repository:  PageRepository = page_repository

    def get_page(self):
        return self.page_repository.get_page()

    def update_page_data(self, description, about_us, slogan):
        updated_description = Page(description, about_us, slogan)
        self.page_repository.update_page(updated_description)


class ContactServiceImpl(implements(ContactService)):

    def __init__(self, contact_repository):
        self.contact_repository: ContactRepository = contact_repository

    def get_contact(self):
        return self.contact_repository.get_contact()

    def update_contact_data(self, address, phone, email, **kwargs):
        updated_contact_data = Contact(address, phone, email, **kwargs)
        return self.contact_repository.update_contact(updated_contact_data)


class EssentialAssetsServiceImpl(implements(EssentialAssetsService)):

    def __init__(self, assets_repository):
        self.assets_repository: EssentialAssetsRepository = assets_repository

    def get_assets(self):
        return self.assets_repository.get_assets()

    def update_assets_data(self,  logo_url, banner_url):
        updated_assets_data = EssentialAssets(logo_url, banner_url)
        return self.assets_repository.update_assets(updated_assets_data)


class ProductPropertiesServiceImpl(implements(ProductPropertiesService)):

    def __init__(self, properties_repository):
        self.properties_repository: ProductPropertiesRepository = properties_repository

    def get_properties(self):
        return self.properties_repository.get_properties()


class CssDataServiceImpl(implements(CssDataService)):
    def __init__(self, css_repository):
        self.css_repository: CssDataRepository = css_repository

    def get_css_path(self):
        return self.css_repository.get_css_path()

    def update_css_data(self, data):
        return self.css_repository.update_css_data(data)

    def get_theme(self):
        return jsonify(self.css_repository.get_theme())


class AccountServiceImpl(implements(AccountService)):

    def __init__(self, account_repository):
        self.account_repository: AccountRepository = account_repository

    def get_account(self):
        return self.account_repository.get_account()

    def update_account_data(self, bank_name, account_owner, account_number, **kwargs):
        updated_account_data = Account(
            bank_name, account_owner, account_number, **kwargs)
        return self.account_repository.update_account(updated_account_data)
