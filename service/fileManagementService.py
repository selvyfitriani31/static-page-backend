import os
import json
from settings.constants import JSON_DATA

def open_file(file_name):
    return open(get_file_path(file_name))

def get_file_path(file_name):
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    print(os.path.join(BASE_DIR, file_name))
    return os.path.join(BASE_DIR, file_name)

def write_json_files(file_name, json_data):
    json_file = open(get_file_path(file_name), 'w')
    json.dump(json_data,json_file, sort_keys=True, indent=4)
    json_file.close()

def open_json_data(filename=JSON_DATA):
    json_file = open_file(filename)
    data = json.load(json_file)
    return data

def is_path_exists(file_path):
    file_path = get_file_path(file_path)
    return os.path.exists(file_path)

def create_dir(dir_path):
    file_path = get_file_path(dir_path)
    os.makedirs(str(file_path))
