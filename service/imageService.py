import os
from werkzeug.utils import secure_filename
from settings.constants import IMAGE_DIR, DEFAULT_IMAGE_DIR
from service.fileManagementService import get_file_path, is_path_exists, create_dir
import logging

log = logging.getLogger(__name__)

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}


def get_image_files_endpoint(filename):
    url_routing = '/apiimage/' + filename
    log.info('Image routing at ' + url_routing)
    return url_routing


def get_image_file_path(file_name):
    files_path = get_file_path(os.path.join(IMAGE_DIR, file_name))
    return files_path


def get_image(file_name):
    if is_path_exists(os.path.join(IMAGE_DIR, file_name)):
        return get_image_file_path(file_name)
    raise FileNotFoundError


def save_image(image_data):
    image_file_name = ''
    check_or_create_if_not_exists_path()
    if check_files_extension(image_data.filename):
        file_save_path = get_image_file_path(image_data.filename)
        image_data.save(str(file_save_path))
        image_file_name = image_data.filename
        log.info('Successfully save image data')
    else:
        log.info('File extension not allowed, return default image')
        image_file_name = return_default_image()
    return image_file_name


def check_or_create_if_not_exists_path():
    if not is_path_exists(IMAGE_DIR):
        create_dir(IMAGE_DIR)


def check_files_extension(file_data):
    return '.' in file_data and \
           file_data.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def check_image_extension(file_data):
    temp_result = check_files_extension(file_data)
    str_result = str(temp_result)
    return str_result


def return_default_image():
    return 'default.png'


def upload_images(files_request):
    images_url = []
    for file_header in files_request.files:
        image_data = files_request.files[file_header]
        image_name = save_image(image_data)
        images_url.append(get_image_files_endpoint(image_name))
    return images_url


def upload_image(files_request):
    key_list_data = []
    for key_name in files_request.files:
        key_list_data.append(key_name)
    request_key = key_list_data[0]
    image_data = files_request.files[request_key]
    image_name = save_image(image_data)
    log.info('Done Saving image')
    return get_image_files_endpoint(image_name)
