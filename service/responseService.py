from service.JsonEncoder import JsonEncoder
from flask import jsonify, json as flask_json, send_file
from factory.ServiceFactory import ServiceFactory
from service.cliService import run_refresh_nginx
from service.services import *
from .imageService import get_image
import logging
import json

log = logging.getLogger(__name__)

service_factory = ServiceFactory()
page_service: PageService = service_factory.get_page_service()
contact_service: ContactService = service_factory.get_contact_service()
assets_service: EssentialAssetsService = service_factory.get_assets_service()
properties_service: ProductPropertiesService = service_factory.get_properties_service()
css_service: CssDataService = service_factory.get_css_service()
account_service: AccountService = service_factory.get_account_service()


def create_response(data, default=JsonEncoder().encode):
    response_data = json.dumps(data, default=default, indent=5, sort_keys=True)
    response_data = json.loads(response_data)
    print(response_data)
    print(type(response_data))
    # Post with void return type
    if response_data is None:
        return jsonify(status=200,
                       text='OK')
    return jsonify(response_data)


def create_failed_response(text='Failed'):
    return jsonify(status=500,
                   text=text)


def send_css_response():
    return send_file(css_service.get_css_path(), mimetype='text/css')


def send_image_data(filename):
    file_path = get_image(filename)
    log.info('Successfully get image data')
    mimetype = "application/octet-stream"
    if ".jpg" in file_path[-5:] or ".jpeg" in file_path[-5:]:
        mimetype = "image/jpeg"
    elif ".png" in file_path[-5:]:
        mimetype = "image/png"
    return send_file(file_path, mimetype=mimetype)


def merge_responses(list_response):

    for index in range(len(list_response)):
        list_response[index] = flask_json.loads(list_response[index].data)

    merged_response = list_response[0]
    for index in range(1, len(list_response)):
        merged_response.update(list_response[index])

    return merged_response


def create_all_data_response(*args):
    args = list(args)
    return merge_responses(args)


def refresh_nginx():
    properties = properties_service.get_properties()
    return run_refresh_nginx(properties)
