from interface import Interface


class PageService(Interface):

    def get_page(self):
        pass

    def update_page_data(self, description, about_us, slogan):
        pass


class ContactService(Interface):

    def get_contact(self):
        pass

    def update_contact_data(self, address, phone, email, **kwargs):
        pass


class EssentialAssetsService(Interface):

    def get_assets(self):
        pass

    def update_assets_data(self, logo_url, banner_url):
        pass


class ProductPropertiesService(Interface):

    def get_properties(self):
        pass


class CssDataService(Interface):

    def get_css_path(self):
        pass

    def update_css_data(self, data):
        pass

    def get_theme(self):
        pass


class AccountService(Interface):
    def get_account(self):
        pass

    def update_account_data(self, bank_name, account_owner, account_number, **kwargs):
        pass
