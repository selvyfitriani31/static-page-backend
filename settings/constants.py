import sys
import os

PLATFORM = sys.platform

# JSON directory
JSON_DIR = os.path.join('static', 'json')
JSON_FILENAME = 'data.json'
JSON_DATA = os.path.join(JSON_DIR, JSON_FILENAME)

# CSS directory
CSS_DIR = os.path.join('static', 'css')

# CLI directory
PYTHON_EXEC = ['python'] if PLATFORM == 'win32' else []
CLI_DIR = '/var/www/engine/cli/'
CLI_FILE = 'runner.py'
CLI_PATH = os.path.join(CLI_DIR, CLI_FILE)
PRODUCT_JSON_FILE = 'properties.json'

# Image directory
IMAGE_DIR = os.path.join('upload', 'image')
DEFAULT_IMAGE_DIR = os.path.join('upload', 'image', 'default.png')
